#!/usr/bin/env python

from images import size, read_img, write_img, create_blank


def sepia_filter(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """Crea una nueva imagen aplicando un filtro sepia.

    # Parametros de entrada
    image: Imagen original (matriz de tuplas RGB).

    # Parametros de salida
    Nueva imagen con filtro sepia.
    """

    # Obtener las dimensiones de la imagen original
    width, height = size(image)

    # Crea una imagen vacia con la anchura y altura de la imagen original
    sepia_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            r, g, b = image[x][y]
            new_r = int(0.393 * r + 0.769 * g + 0.189 * b)
            new_g = int(0.349 * r + 0.686 * g + 0.168 * b)
            new_b = int(0.272 * r + 0.534 * g + 0.131 * b)
            sepia_image[x][y] = (min(new_r, 255), min(new_g, 255), min(new_b, 255))

    return sepia_image


def adjust_brightness(image: list[list[tuple[int, int, int]]], factor: float) -> list[list[tuple[int, int, int]]]:
    """Ajusta la luminosidad de la imagen multiplicando los componentes RGB por un factor.

    # Parametros de entrada
    image: Imagen original (matriz de tuplas RGB).
    factor: Factor de ajuste de luminosidad.

    # Parametros de salida
    Nueva imagen con ajuste de luminosidad.
    """

    # Obtener las dimensiones de la imagen original
    width, height = size(image)

    # Crea una imagen vacia con la anchura y altura de la imagen original
    bright_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            r, g, b = image[x][y]
            new_r = int(r * factor)
            new_g = int(g * factor)
            new_b = int(b * factor)
            bright_image[x][y] = (min(new_r, 255), min(new_g, 255), min(new_b, 255))

    return bright_image


def adjust_contrast(image: list[list[tuple[int, int, int]]], factor: float) -> list[list[tuple[int, int, int]]]:
    """Ajusta el contraste de la imagen multiplicando y desplazando los componentes RGB.

    # Parametros de entrada
    image: Imagen original (matriz de tuplas RGB).
    factor: Factor de ajuste de contraste.

    # Parametros de salida
    Nueva imagen con ajuste de contraste.
    """

    # Obtener las dimensiones de la imagen original
    width, height = size(image)

    # Crea una imagen vacia con la anchura y altura de la imagen original
    contrast_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            r, g, b = image[x][y]
            new_r = int((r - 128) * factor + 128)
            new_g = int((g - 128) * factor + 128)
            new_b = int((b - 128) * factor + 128)
            contrast_image[x][y] = (min(new_r, 255), min(new_g, 255), min(new_b, 255))

    return contrast_image


def negative(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """Crea una nueva imagen aplicando el efecto de negativo.

    # Parametros de entrada
    image: Imagen original (matriz de tuplas RGB).

    # Parametros de salida
    Nueva imagen con efecto negativo.
    """

    # Obtener las dimensiones de la imagen original
    width, height = size(image)

    # Crea una imagen vacia con la anchura y altura de la imagen original
    negative_image = create_blank(width, height)

    for x in range(width):
        for y in range(height):
            r, g, b = image[x][y]
            negative_image[x][y] = (255 - r, 255 - g, 255 - b)

    return negative_image


def main():
    # Leer imagen 'cafe.jpg'
    imagen_original = read_img("cafe.jpg")

    # Uso de la funcion sepia_filter
    imagen_sepia = sepia_filter(imagen_original)
    write_img(imagen_sepia, "sepia_filter.jpg")

    # Uso de la funcion adjust_brightness
    imagen_brillo = adjust_brightness(imagen_original, 5)
    write_img(imagen_brillo, "adjust_brightness.jpg")

    # Uso de la funcion adjust_contrast
    imagen_contraste = adjust_contrast(imagen_original, -3)
    write_img(imagen_contraste, "adjust_contrast.jpg")

    # Uso de la funcion negative
    imagen_negativa = negative(imagen_original)
    write_img(imagen_negativa, "negative.jpg")


if __name__ == "__main__":
    main()
