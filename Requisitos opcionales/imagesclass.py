#!/usr/bin/env python

from PIL import Image


class ImageClass:
    def __init__(self, pixels_xy):  # Varible para obtener la tupla de los componentes de la imagen
        self.pixels_xy = pixels_xy

    def size(self):
        """Devuelve el tamaño de la imagen."""
        width = len(self.pixels_xy)
        height = len(self.pixels_xy[0])
        return width, height

    @classmethod  # Se usa para poder llamar directamente al metodo y no crear una nueva instancia antes
    def read_img(cls, filename):
        """Lee una imagen de un archivo y crea una instancia de la clase ImageClass."""
        img = Image.open(filename)
        width, height = img.size

        pixels = img.getdata()
        pixels_xy = []
        for x in range(width):
            pixels_xy.append([0] * height)
            for y in range(height):
                pixels_xy[x][y] = pixels[(y * width) + x]

        return cls(pixels_xy)

    def write_img(self, filename):
        """Escribe la imagen en un archivo."""
        width, height = self.size()  # El tamaño se obtiene usando el metodo size
        pixels = [0] * width * height
        for x in range(width):
            for y in range(height):
                pixels[(y * width) + x] = self.pixels_xy[x][y]

        img = Image.new(mode='RGB', size=(width, height))
        img.putdata(pixels)
        img.save(filename)

    @classmethod
    def create_blank(cls, width, height):
        """Crea una imagen vacía con la anchura y altura dadas."""
        pixels = []
        for _ in range(width):  # Mejor poner _ ya que la 'x' no se usaba
            pixels.append([(0, 0, 0)] * height)
        return cls(pixels)


image = ImageClass.read_img('cafe.jpg')
image.write_img('cafe_class.jpg')
