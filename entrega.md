# ENTREGA CONVOCATORIA ENERO
Laura del Pilar Machado Sanchez<br>
lp.machado.2020@alumnos.urjc.es

# Apartado para adjuntar enlace del video
Recomiendo verlo a 1080p, si no los detalles de algunas imagenes se pierden.<br>
https://youtu.be/t9RLVSky-60

# Requisitos mínimos

Todas las imágenes generadas se encuentran dentro de la carpeta 'Imagenes'.

Programa **transforms.py**:
- Método **change_colors**. Funciona correctamente. Sin embargo, en la imagen del café he probado varios colores pero no se observan cambios. Con la imagen llamada '*ejemplo.jpg*' si cambio el color blanco a negro si se observan los cambios esperados. Imagen '*change_colors_ejemplo.jpg*'
- Método **rotate_right**. Funciona correctamente.
- Método **mirror**. Funciona correctamente.
- Método **rotate_colors**. Funciona correctamente.
- Método **blur**. Funciona correctamente.
- Método **shift**. Funciona correctamente.
- Método **crop**. Funciona correctamente.
- Método **grayscale**. Funciona correctamente.
- Método **filter**. Funciona correctamente.

Programa **transform_simple.py**. Funciona correctamente.<br>
Programa **trasnsform_args.py**. Funciona correctamente.<br>
Programa **transform_multi.py**. Funciona correctamente.

# Requisitos opcionales

Programa **transforms_extra.py**:
- Método **sepia_filter**. Funciona correctamente.
- Método **adjust_brightness**. Funciona correctamente.
- Método **adjust_contrast**. Funciona correctamente.
- Método **negative**. Funciona correctamente.

Programa **imagesclass.py**:
- Clase **ImageClass**. Métodos *size*, *read_img*, *write_img* y *create_blank*. He cambiado el nombre de la clase porque al usar Image como lo importamos de Pillow, me genera error al usar el mismo nombre.

# Requisitos opcionales propios

Programa **metodo_shift.py**:
- He creado un nuevo metodo *shift*, con el mismo funcionamiento que el del programa **transforms.py**, 
pero el sobrante de la imagen no lo añadimos de nuevo. Se queda de color negro lo que desplazamos y sigue manteniendo las dimensiones del imagen original.<br>

Esta extension no aparece en el video de demostracion, pero se ejecuta como los metodos del archivo **transforms.py**.
La imagen resultado se llama "*black-shift.jpf*".

He decidido añadirlo por el correo informativo sobre como hacer el metodo shift. En mi programa **transforms.py** el sobrante se añade de nuevo introduciendo como su fuera una imagen infinita. 