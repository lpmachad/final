from images import *

def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    """Desplazará la imagen el número de píxeles que se indique en el eje horizontal o vertical.

    # Parametros de entrada
    image: Imagen original (matriz de tuplas RGB).
    horizontal: Cantidad de píxeles a desplazar horizontalmente (positivo a la derecha, negativo a la izquierda).
    vertical: Cantidad de píxeles a desplazar verticalmente (positivo hacia arriba, negativo hacia abajo).

    # Parametros de salida
    Nueva imagen desplazada.
    """

    # Obtener las dimensiones de la imagen original
    width, height = size(image)

    # Crea una imagen vacía con la anchura y altura de la imagen original
    shifted_image = create_blank(width, height)

    # Iterar sobre cada píxel y calcular la nueva posición
    for x in range(width):
        for y in range(height):
            # Calcular las nuevas coordenadas
            new_x = x - horizontal
            new_y = y - vertical

            # Verificar si las nuevas coordenadas están dentro de las dimensiones originales
            if 0 <= new_x < width and 0 <= new_y < height:
                # Asignar el valor original a la nueva posición si está dentro de las dimensiones
                shifted_image[new_x][new_y] = image[x][y]
            else:
                # Si está fuera, asignar negro
                shifted_image[x][y] = (0, 0, 0)

    return shifted_image


def main():
    imagen_original = read_img("cafe.jpg")

    # Uso de la función shift
    imagen_desplazada = shift(imagen_original, 20, -40)
    write_img(imagen_desplazada, "shift-black.jpg")


if __name__ == "__main__":
    main()
