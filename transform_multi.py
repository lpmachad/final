#!/usr/bin/env python

from transforms import change_colors, rotate_right, mirror, rotate_colors, blur, shift, crop, grayscale, filter
from images import read_img, write_img
import sys


def apply_transformations(transformed_image, transformations):
    i = 0
    while i < len(transformations):
        transformation = transformations[i]
        i += 1
        if transformation == 'change_colors':
            color_from = tuple(int(x) for x in transformations[i:i + 3])    # De i a 'i+3'
            i += 3  # Incrementa en tres para leer lo que venga despues de los argumentos
            color_to = tuple(int(x) for x in transformations[i:i + 3])      # De i a 'i+3'
            i += 3
            transformed_image = change_colors(transformed_image, color_from, color_to)
        elif transformation == 'rotate_right':
            transformed_image = rotate_right(transformed_image)
        elif transformation == 'mirror':
            transformed_image = mirror(transformed_image)
        elif transformation == 'rotate_colors':
            increment = int(transformations[i])
            i += 1
            transformed_image = rotate_colors(transformed_image, increment)
        elif transformation == 'blur':
            transformed_image = blur(transformed_image)
        elif transformation == 'shift':
            horizontal = int(transformations[i])
            i += 1
            vertical = int(transformations[i])
            i += 1
            transformed_image = shift(transformed_image, horizontal, vertical)
        elif transformation == 'crop':
            x, y, width, height = [int(x) for x in transformations[i:i + 4]]    # De i a 'i+4'       
            i += 4  # Incrementa en cuatro para leer lo que venga despues de los argumentos
            transformed_image = crop(transformed_image, x, y, width, height)
        elif transformation == 'grayscale':
            transformed_image = grayscale(transformed_image)
        elif transformation == 'filter':
            r, g, b = [float(x) for x in transformations[i:i + 3]]  # De i a 'i+3'
            i += 3
            transformed_image = filter(transformed_image, r, g, b)
        else:
            print(f"Error: Método de transformación '{transformation}' no válido.")
            sys.exit(1)

    return transformed_image


def main():
    if len(sys.argv) < 4:
        print("Error: Se esperan al menos tres argumentos en la línea de comandos.")
        print("Uso: python3 transform_multi.py <nombre_fichero> <transformacion1> [arg1 arg2 ...] "
              "<transformacion2> [arg1 arg2 ...] ...")
        sys.exit(1)

    imagen_name = sys.argv[1]
    transformations = sys.argv[2:]

    try:
        original_image = read_img(imagen_name)
    except FileNotFoundError:
        print(f"Error: El archivo {imagen_name} no existe.")
        sys.exit(1)

    transformed_image = apply_transformations(original_image, transformations)

    name = imagen_name.split(".")[0]
    extension = imagen_name.split(".")[1]

    write_img(transformed_image, f"Imagenes/{name}_trans.{extension}")


if __name__ == "__main__":
    main()
