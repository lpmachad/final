#!/usr/bin/env python

from images import size, read_img, write_img, create_blank


def change_colors(image: list[list[tuple[int, int, int]]],
                  to_change: tuple[int, int, int],
                  to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    """Cambiar los colores en una imagen.
    
    # Parametros de entrada
    image: Imagen original (matriz de tuplas RGB).
    to_change: Color a cambiar.
    to_change_to: Nuevo color.
    
    # Parametros de salida
    Nueva imagen con los colores cambiados.
    """

    # Obtener las dimensiones de la imagen original
    width, height = size(image)

    # Crear una copia de la imagen original
    new_image = []  # Crear una nueva lista vacía

    # Iterar sobre cada fila en la imagen original
    for row in image:
        # Crear una copia de la fila y agregarla a la nueva lista
        new_row = row.copy()
        new_image.append(new_row)

    # Iterar sobre cada píxel y cambiar el color si coincide con el color a cambiar
    for x in range(width):
        for y in range(height):
            if image[x][y] == to_change:    # Si el color que queremos cambiar lo encuentra
                new_image[x][y] = to_change_to  # Se cambia por el color elegido

    return new_image


def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """Girar imagen 90 grados hacia la derecha.

    # Parametros de entrada
    image: Imagen original (matriz de tuplas RGB).

    # Parametros de salida
    Imagen girada 90 grados hacia la derecha.
    """

    # Obtener las dimensiones de la imagen original
    width, height = size(image)

    # Crea una imagen vacia con la anchura y altura de la imagen original
    rotated_image = create_blank(height, width)     # Por el giro el ancho es el largo y el largo es el ancho

    # Copiar los píxeles girados en la nueva matriz rotated_image
    for x in range(width):
        for y in range(height):
            # Las 'y' ahora son las 'x' y las 'x' han cambiado de dimensiones a 'width - 1 - x'
            rotated_image[y][width - 1 - x] = image[x][y]

    return rotated_image


def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """Genera una nueva imagen en espejo respecto al eje vertical.
    
    # Parametros de entrada
    image: Imagen original (matriz de tuplas RGB).
    
    # Parametros de salida
    Imagen en espejo respecto al eje vertical.
    """

    # Obtener las dimensiones de la imagen original
    width, height = size(image)

    # Crea una imagen vacia con la anchura y altura de la imagen original
    mirrored_image = create_blank(width, height)

    # Copiar los píxeles espejados en la nueva matriz
    for x in range(width):
        for y in range(height):
            mirrored_image[x][y] = image[width - 1 - x][y]  # En este caso cambian las 'x' de posicion a 'width - 1 - x'

    return mirrored_image


def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    """Genera una nueva imagen con los colores cambiados según un incremento.

    # Parametros de entrada
    image: Imagen original (matriz de tuplas RGB).
    increment: Incremento a aplicar a los valores RGB de cada píxel.

    # Parametros de salida
    Nueva imagen con colores cambiados.
    """

    # Obtener las dimensiones de la imagen original
    width, height = size(image)

    # Crea una imagen vacia con la anchura y altura de la imagen original
    rotated_colors_image = create_blank(width, height)

    # Rellenar la matriz con colores cambiados
    for x in range(width):
        for y in range(height):
            rotated_color = []
            for component in image[x][y]:
                rotated_component = (component + increment) % 256   # Se incrementa módulo 256
                rotated_color.append(rotated_component)
            rotated_colors_image[x][y] = tuple(rotated_color)

    return rotated_colors_image


def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """Crea una nueva imagen borrosa. El valor RGB de cada píxel es el valor medio de los píxeles
    que tiene encima, debajo, a la derecha y a la izquierda.

    # Parametros de entrada
    image: Imagen original (matriz de tuplas RGB).

    # Parametros de salida
    Nueva imagen borrosa.
    """

    # Obtener las dimensiones de la imagen original
    width, height = size(image)

    # Crea una imagen vacia con la anchura y altura de la imagen original
    blurred_image = create_blank(width, height)

    # Iterar sobre cada píxel y calcular el valor medio de los píxeles alrededor
    for x in range(width):
        for y in range(height):
            neighbors = []
            if x > 0:
                neighbors.append(image[x - 1][y])   # Comprobar píxel izquierdo
            if x < width - 1:
                neighbors.append(image[x + 1][y])   # Comprobar píxel derecho
            if y > 0:
                neighbors.append(image[x][y - 1])   # Comprobar píxel superior
            if y < height - 1:
                neighbors.append(image[x][y + 1])   # Comprobar píxel inferior

            # Calcular el valor medio de los valores RGB
            if neighbors:
                sum_rgb = [0, 0, 0]  # Inicializar la lista para sumar los componentes R, G, B

                for i in range(3):
                    sum_component = 0
                    # Iterar sobre los píxeles alrededor
                    for c in neighbors:
                        sum_component += c[i]

                    sum_rgb[i] = sum_component

                # Calcular el valor medio de los componentes RGB
                average_rgb = (sum_rgb[0] // len(neighbors), sum_rgb[1] // len(neighbors), sum_rgb[2] // len(neighbors))
                blurred_image[x][y] = average_rgb
            else:
                # Si el píxel está en un borde, mantener el valor original
                blurred_image[x][y] = image[x][y]

    return blurred_image


def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    """Desplazará la imagen el número de pixels que se indique en el eje horizontal o vertical.

    # Parametros de entrada
    image: Imagen original (matriz de tuplas RGB).
    horizontal: Cantidad de píxeles a desplazar horizontalmente (positivo a la derecha, negativo a la izquierda).
    vertical: Cantidad de píxeles a desplazar verticalmente (positivo hacia arriba, negativo hacia abajo).

    # Parametros de salida
    Nueva imagen desplazada.
    """

    # Obtener las dimensiones de la imagen original
    width, height = size(image)

    # Crea una imagen vacia con la anchura y altura de la imagen original
    shifted_image = create_blank(width, height)

    # Iterar sobre cada píxel y calcular la nueva posición
    for x in range(width):
        for y in range(height):
            # Calcular las nuevas coordenadas
            # Usando el modulo nos aseguramos que la nueva imagen este dentro de las dimensiones width y height
            new_x = (x + horizontal) % width
            new_y = (y + vertical) % height

            # Asignar el valor original a la nueva posición
            shifted_image[new_x][new_y] = image[x][y]

    return shifted_image


def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int)\
        -> list[list[tuple[int, int, int]]]:
    """Crea una nueva imagen recortada.

    # Parametros de entrada
    image: Imagen original (matriz de tuplas RGB).
    x: Coordenada x de la esquina superior izquierda del rectángulo.
    y: Coordenada y de la esquina superior izquierda del rectángulo.
    width: Ancho del rectángulo.
    height: Altura del rectángulo.

    # Parametros de salida
    Nueva imagen recortada.
    """

    # Obtener las dimensiones de la imagen original
    original_width, original_height = size(image)

    # Verificar que las coordenadas del rectángulo estén dentro de los límites
    if x < 0 or y < 0 or x + width > original_width or y + height > original_height:
        raise ValueError("Las coordenadas del rectángulo están fuera de los límites de la imagen.")

    # Crea una imagen vacia con la anchura y altura de la imagen recortada
    cropped_image = create_blank(width, height)

    # Iterar sobre el rectángulo y copiar los píxeles dentro de los límtes
    for i in range(width):
        for j in range(height):
            cropped_image[i][j] = image[x + i][y + j]

    return cropped_image


def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    """Crea una nueva imagen en escala de grises.

    # Parametros de entrada
    image: Imagen original (matriz de tuplas RGB).

    # Parametros de salida
    Nueva imagen en escala de grises.
    """

    # Obtener las dimensiones de la imagen original
    width, height = size(image)

    # Crea una imagen vacia con la anchura y altura de la imagen original
    grayscale_image = create_blank(width, height)

    # Iterar sobre cada píxel y convertir a escala de grises
    for x in range(width):
        for y in range(height):
            average_value = sum(image[x][y]) // 3   # Calcular media
            # Asignar el valor medio a cada componente
            grayscale_image[x][y] = (average_value, average_value, average_value)

    return grayscale_image


def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    """Crea una nueva imagen con un filtro.

    # Parametros de entrada
    image: Imagen original (matriz de tuplas RGB).
    r: Multiplicador de filtro para el componente rojo.
    g: Multiplicador de filtro para el componente verde.
    b: Multiplicador de filtro para el componente azul.

    # Parametros de salida
    Nueva imagen con el filtro.
    """

    # Obtener las dimensiones de la imagen original
    width, height = size(image)

    # Crea una imagen vacia con la anchura y altura de la imagen original
    filtered_image = create_blank(width, height)

    # Iterar sobre cada píxel y aplicar el filtro
    for x in range(width):
        for y in range(height):
            filtered_pixel = (min(int(image[x][y][0] * r), 255),
                              min(int(image[x][y][1] * g), 255),
                              min(int(image[x][y][2] * b), 255))
            filtered_image[x][y] = filtered_pixel

    return filtered_image


def main():
    # Leer imagen 'cafe.jpg'
    imagen_original = read_img("cafe.jpg")

    # Uso de la funcion change_colors
    # De blanco a negro
    imagen_colores_cambiados = change_colors(imagen_original, (255, 255, 255), (0, 0, 0))
    write_img(imagen_colores_cambiados, "Imagenes/change_colors.jpg")

    imagen_ejemplo = read_img("Imagenes/ejemplo.jpg")
    # De blanco a negro
    imagen_ejemplo_colores = change_colors(imagen_ejemplo, (255, 255, 255), (0, 0, 0))
    write_img(imagen_ejemplo_colores, "Imagenes/change_colors_ejemplo.jpg")

    # Uso de la funcion rotate_right
    imagen_giro_dercha = rotate_right(imagen_original)
    write_img(imagen_giro_dercha, "Imagenes/rotate_right.jpg")

    # Uso de la funcion mirror
    imagen_espejada = mirror(imagen_original)
    write_img(imagen_espejada, "Imagenes/mirror.jpg")

    # Uso de la funcion rotate_colors
    imagen_colores_rotados = rotate_colors(imagen_original, 700)
    write_img(imagen_colores_rotados, "Imagenes/rotate_colors.jpg")

    # Uso de la funcion blur
    imagen_difuminada = blur(imagen_original)
    write_img(imagen_difuminada, "Imagenes/blur.jpg")

    # Uso de la funcion shift
    imagen_desplazada = shift(imagen_original, 60, -80)
    write_img(imagen_desplazada, "Imagenes/shift.jpg")

    # Uso de la funcion crop
    imagen_recortada = crop(imagen_original, 50, 50, 600, 500)
    write_img(imagen_recortada, "Imagenes/crop.jpg")

    # Uso de la funcion grayscale
    imagen_gris = grayscale(imagen_original)
    write_img(imagen_gris, "Imagenes/grayscale.jpg")

    # Uso de la funcion filter
    imagen_filtrada = filter(imagen_original, 1, 2, 3)
    write_img(imagen_filtrada, "Imagenes/filter.jpg")


if __name__ == "__main__":
    main()
