#!/usr/bin/env python

from transforms import change_colors, rotate_right, mirror, rotate_colors, blur, shift, crop, grayscale, filter
from images import read_img, write_img
import sys


def get_arguments():
    try:
        methods = ["change_colors", "rotate_right", "mirror", "rotate_colors", "blur", "shift", "crop", "grayscale", "filter"]
        imagen_name = sys.argv[1]
        transformation = sys.argv[2]

        # Si no se encuentra dentro de los métodos, imprime un error
        if transformation not in methods:
            print(f"Error: Método de transformación '{transformation}' no válido.")
            sys.exit(1)

        methods_with_argv = ["change_colors", "rotate_colors", "shift", "crop", "filter"]
        argv = None  # Inicializar argv en caso de que no sea necesario
        if transformation in methods_with_argv:
            argv = [int(arg) for arg in sys.argv[3:]]  # Convertir parámetros a enteros

    # Maneja los errores si no se introducen argumentos o argumentos erróneos
    except IndexError:
        print("Error: Se esperan dos argumentos en la línea de comandos.")
        print("Uso: python3 transform_args.py <nombre_fichero> <transformacion> [arg1 arg2 ...]")
        sys.exit(1)

    return imagen_name, transformation, argv


def apply_transformation():
    imagen_name, transformation, argv = get_arguments()
    name = imagen_name.split(".")[0]
    extension = imagen_name.split(".")[1]

    # Si no encuentra el archivo de la imagen, imprime un error
    try:
        original_image = read_img(imagen_name)
    except FileNotFoundError:
        print(f"Error: El archivo {imagen_name} no existe.")
        sys.exit(1)

    if transformation == 'change_colors':
        transformed_image = change_colors(original_image, (argv[0], argv[1], argv[2]),
                                          (argv[3], argv[4], argv[5]))  # Color que queremos cambiar, color para cambiar
    elif transformation == 'rotate_right':
        transformed_image = rotate_right(original_image)
    elif transformation == 'mirror':
        transformed_image = mirror(original_image)
    elif transformation == 'rotate_colors':
        transformed_image = rotate_colors(original_image, argv[0])     # Incremento
    elif transformation == 'blur':
        transformed_image = blur(original_image)
    elif transformation == 'shift':
        transformed_image = shift(original_image, argv[0], argv[1])    # Horizontal y vertical
    elif transformation == 'crop':
        transformed_image = crop(original_image, argv[0], argv[1], argv[2], argv[3])   # 'x', 'y', width, height
    elif transformation == 'grayscale':
        transformed_image = grayscale(original_image)
    elif transformation == 'filter':
        transformed_image = filter(original_image, argv[0], argv[1], argv[2])     # 'r', 'g', 'b'
    else:
        print(f"Error: Método de transformación '{transformation}' no válido.")
        sys.exit(1)

    # Como es común a todas las condiciones, se puede poner fuera y así ahorramos líneas de código
    write_img(transformed_image, f"Imagenes/{name}_trans.{extension}")


def main():
    apply_transformation()


if __name__ == "__main__":
    main()
