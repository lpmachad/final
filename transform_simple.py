#!/usr/bin/env python

from transforms import rotate_right, mirror, blur, grayscale
from images import read_img, write_img
import sys


def get_arguments():
    try:
        methods = ["rotate_right", "mirror", "blur", "grayscale"]
        imagen_name = sys.argv[1]
        transformation = sys.argv[2]

        # Si no se encuentra dentro de los metodos imprime un error
        if transformation not in methods:
            print(f"Error: Método de transformación '{transformation}' no válido.")
            sys.exit(1)

    # Maneja los errores si no se introducen argumentos o argumentos erroneos
    except IndexError:
        print("Error: Se esperan dos argumentos en la línea de comandos.")
        print("Uso: python3 transform_simple.py <nombre_fichero> <transformacion>")
        sys.exit(1)

    return imagen_name, transformation


def apply_transformation():
    imagen_name, transformation = get_arguments()
    name = imagen_name.split(".")[0]
    extension = imagen_name.split(".")[1]

    # Si no encuentra el archivo de la imagen imprime un error
    try:
        imagen_original = read_img(imagen_name)
    except FileNotFoundError:
        print(f"Error: El archivo {imagen_name} no existe.")
        sys.exit(1)

    if transformation == 'rotate_right':
        transformed_image = rotate_right(imagen_original)
    elif transformation == 'mirror':
        transformed_image = mirror(imagen_original)
    elif transformation == 'blur':
        transformed_image = blur(imagen_original)
    elif transformation == 'grayscale':
        transformed_image = grayscale(imagen_original)
    else:
        print(f"Error: Método de transformación '{transformation}' no válido.")
        sys.exit(1)

    # Como es comun a todas las condiciones se puede poner fuera y asi ahorramos lineas de codigo
    write_img(transformed_image, f"Imagenes/{name}_trans.{extension}")


def main():
    apply_transformation()


if __name__ == "__main__":
    main()
