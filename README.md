## Instrucciones para ejecutar cada archivo:

### Requisitos minimos

- transforms.py
```
python3 transforms.py
```
- transform_simple.py
```
python3 transform_simple.py <nombre_fichero> <transformacion>
```
- trasnsform_args.py
```
python3 trasnsform_args.py <nombre_fichero> <transformacion> [arg1 arg2 ...]
```
- trasnsform_multi.py
```
python3 trasnsform_multi.py <nombre_fichero> <transformacion> [arg1 arg2 ...] <nombre_fichero> <transformacion> [arg1 arg2 ...]
```
##

### Requisitos opcionales

- transforms_extra.py
```
python3 transforms_extra.py
```
- imagesclass.py
```
python3 imagesclass.py
```

##

### Requisitos opcionales propios
- metodo_shift.py
```
python3 metodo_shift.py
```